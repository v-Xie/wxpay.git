// pages/wxpay/wxpay.js
Page({
  data: {
    payTempcode: '',
    ordercode: '',
    payParam: {}
  },
  onLoad: function (options) {
    console.log('支付开始');
    console.log(options);
    this.setData({
      ordercode: options.ordercode
    });
    this.getTempcode();
  },
  // 换取支付临时code
  getTempcode: function () {
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        this.setData({
          payTempcode: res.code
        });
        console.log('支付code:', this.data.payTempcode);

        this.getPayinfo();
      }
    })
  },
  // 换取支付参数
  getPayinfo: function () {
    var self = this;
    wx.request({
      url: 'https://xxxxxx/pay/xcxpay',//后台接口地址
      data: {
        'wxcode': self.data.payTempcode,
        'ordercode': self.data.ordercode,
        'gid': x,
      },
      method: 'POST',
      success: function (res) {
        console.log(res.data.data.payinfo);
        self.setData({
          payParam: res.data.data.payinfo
        });
        console.log('支付的订单====', self.data.ordercode);
        // 调起支付
        wx.requestPayment({
          'timeStamp': self.data.payParam.time,//为字符串，否则报错
          'nonceStr': self.data.payParam.nonce_str,
          'package': `prepay_id=${self.data.payParam.prepay_id}`,
          'signType': 'MD5',
          'paySign': self.data.payParam.paysign,
          'success': function (res) {
            console.log(res)
            console.log('=======支付成功==========');
            wx.navigateTo({
              url: `/pages/lnyc2019/detail?ordercode=${self.data.ordercode}`
            })
          },
          'fail': function (res) {
            console.log(res)
            console.log('=======支付失败==========')
            wx.navigateBack({
              delta: 1//返回1个页面
            })
          }
        })
      }
    })
  }


})